# Infrastructure

A set of scripts and tools I use to setup my personal infrastructure.

## Requirements and setup

## Install requirements on local machine

* GoCryptFS
* Terraform
* Ansible

## First add/setup the Ansible roles, which are set are gitsubmodules.

* Adding a submodule:

```
git submodule add <submodule-url> ansible/roles/<name of role>
```

* Setup submodules:

```
cd ansible/roles
git submodule init
git submodule update
```

## Create/edit Ansible vault for group vars

The creation step is for the first setup of the infrastructure.

Ansible needs a vault for groups vars to be created which will contain any "shared" secrets among the servers.

Keep the password of the ansible vault under the local-vault (see next step) because it's a shared secret.

```
ansible-vault encrypt --vault-password-file=../../../local-vault/ansible_vault_password ansible/group_vars/all/vault
```

Once you have created the Ansible vault you can add variables like this:

```
ansible-vault edit ansible/group_vars/all/vault
```

in the following format:

```
main_domain: 'mydomain.com'
vault_a_secret: 'secret'
```

## Create vaults for terraform secrets:

### Local-vault / Shared backend:

This backend includes the secrets that are server-oriented (or other resource oriented) and the terraform state of the server (resource). All servers share their secrets on that same vault.

It lives **encrypted** on my personal storage. So we need to use GoCryptFS to decrypt it and use it.

Inside the shared backend there are directories (backends) for each environment: *global-resources*, *nextcloud*, *jitsi*, etc. Each one of these includes:

- **terraform.tfstate:** State of each environment, describes all the living resources
- **terraform.tfvars:** Includes secrets related to the server

The terraform.tfvars file should follow the format below:

```
luks_password="password-content-here"
```

**How to use:**

- Create a directory named `local-vault` under the root directory of the git repo.

```
mkdir -p ~/path-to-repo/infrastructure/local-vault
```

- If the encrypted shared backend is on Nextcloud, sync Nextcloud client or download the `infrastructure-secrets` directory.

- Use [GoCryptFS](https://github.com/rfjakob/gocryptfs) to decrypt the shared backend file and mount the decrypted files to `local-vault`

```
gocryptfs /path/to/infrastructure-secrets ~/path-to-repo/infrastructure/local-vault
```

- Each environment points to a local backend inside that local-vault: `local-vault/<environment>/terraform.tfstate`. Make sure that this directory exists inside the local-vault.

- When you're done provisioning, *don't forget* to **unmount** the local-vault!

```
sudo umount ~/path-to-repo/infrastructure/local-vault
```

### User vault / Personal secrets

During the provisioning, we need some personal per user/project secrets/configurations to inject into the environments, as terraform variables.

These variables are:
- **Hcloud API token:** API access token for each project.
- **Path to private ssh key:** Path to your private ssh key that terraform will use to connect to the server.

The terraform.tfvars file should follow the format below:

```
project_hcloud_token="token-content-here"
ssh_private_key = "~/.ssh/my-personal-key"
```

**How to use:**

- Create a directory named `vault` under the environment directory that you're provisioning.

```
mkdir -p ~/path-to-repo/infrastructure/terraform/production/<environment>/vault
```

- Inside the vault create a file named `terraform.tfvars` following the template shown above. Configure it with your personal values.

- When you're done provisioning, *don't forget* to **delete or encrypt** the personal vault directory

## Ready

You're ready to use Terraform to provision your resources.

