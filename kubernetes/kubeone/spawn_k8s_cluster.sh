#!/bin/bash

# This script is using the module k8s-cluster of terraform
# and kubeone provisioning tool for K8s and automatically
# spawns a k8s cluster for you.
#
# Usage: ./spawn_k8s_cluster.sh

(cd ../../terraform/production/k8s-cluster && terraform init && terraform apply -var-file vault/terraform.tfvars -var-file ../../../local-vault/k8s-cluster/terraform.tfvars -auto-approve)

echo "Specify the name of the production environment of k8s cluster created:"
read production_env
 or static import:
#production_env="k8s-cluster"

# Create tf.json of terraform output
(cd ../../terraform/production/${production_env} && terraform output -json > ../../../local-vault/${production_env}/tf.json)


# Export HCLOUD TOKEN
# Is HCLOUD_TOKEN exported?
check_token=$(env | grep HCLOUD_TOKEN)
if [[ $check_token == "" ]]; then
  # Token is not exported
  # Grab it from personal vault and export it if vault is decrypted
  # Env var's scope is only inside this script
  vault="../../terraform/production/${production_env}/vault"
  if [[ -d ${vault} ]]; then
    unset HCLOUD_TOKEN
    grab_token=$(cat ${vault}/terraform.tfvars | grep -m 1 hcloud_token)
    export HCLOUD_TOKEN="$( cut -d'"' -f2 <<<"$grab_token")"
  fi
fi

# Configure kubeone
kubeone apply --manifest kubeone.yaml -t ../../local-vault/${production_env}/tf.json --auto-approve

# Move files to local vault
mv kubernetes.tar.gz ../../local-vault/${production_env}/
mv kubernetes-kubeconfig ../../local-vault/${production_env}/
