variable "hcloud_token" {
  description = "Hetzner API Token for Suite V1 project."
}

variable "ssh_private_key" {
  description = "Private key used by one's ssh agent"
}

variable "ip_range" {
  description = "ip range to use for private network"
}

