terraform {
  backend "local" {
    path = "../../../local-vault/bare-metal-network/terraform.tfstate"
  }
}

module "network" {
  source = "../../modules/network"

  # Personal vault
  hcloud_token = var.hcloud_token
  ssh_private_key = var.ssh_private_key

  # Shared vault
  ip_range = var.ip_range
}
