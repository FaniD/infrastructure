terraform {
  backend "local" {
    path = "../../../local-vault/server-volume/terraform.tfstate"
  }
}

terraform {
  required_providers {
    hcloud = {
      source  = "hetznercloud/hcloud"
      version = ">= 1.20.1"
    }
  }
}

provider "hcloud" {
  token = var.hcloud_token
}

module "base" {
  source = "../../modules/server"

  providers = {
    hcloud = hcloud
  }

  # Shared vault
  net-id = var.net-id

  # Personal vault
  ssh_private_key = var.ssh_private_key
  hcloud_token    = var.hcloud_token

  # Change manually the following vars
  server_name  = "a-server"
  server_os    = "debian"
  server_image = "debian-10"
}

module "storage" {
  source = "../../modules/volume"

  depends_on = [module.base]

  providers = {
    hcloud = hcloud
  }

  # Shared vault
  luks_password = var.luks_password_storage

  # Personal vault
  ssh_private_key = var.ssh_private_key
  hcloud_token    = var.hcloud_token

  # Change manually the following vars
  volume_name = "storage"
  volume_size = "20"
  mountpoint  = "/media/storage"

  # Server module outputs
  server_name     = "${module.base.server-name}"
  server-location = "${module.base.server-location}"
  server-id       = "${module.base.server-id}"
  server-ip       = "${module.base.server-ip}"
}
