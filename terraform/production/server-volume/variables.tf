# ==========================
# Defined in personal vault
# ==========================

# API token and ssh key per project and per user
variable "hcloud_token" {
  description = "Hetzner API Token for a project."
}

variable "ssh_private_key" {
  description = "Private key used by one's ssh agent."
}

# ========================
# Defined in shared vault
# ========================

# This value needs to change according the network id used in the project
# If network was created with global-resources go to local-vault/global-resources
# and run "teraform output"
# Otherwise find the net id through Hetzner / use hcloud cli -> hcloud network list
variable "net-id" {
  description = "Private network id to attach the server to"
}

variable "luks_password_storage" {
  description = "Luks password for volume storage"
}
