terraform {
  backend "local" {
    path = "../../../local-vault/nginx-letsencrypt/terraform.tfstate"
  }
}

terraform {
  required_providers {
    hcloud = {
      source  = "hetznercloud/hcloud"
      version = ">= 1.20.1"
    }
  }
}

provider "hcloud" {
  token = var.hcloud_token
}

module "base" {
  source = "../../modules/server"

  providers = {
    hcloud = hcloud
  }

  # Shared vault
  net-id = var.net-id

  # Personal vault
  ssh_private_key = var.ssh_private_key
  hcloud_token    = var.hcloud_token

  # Change manually the following vars
  server_name  = "test"
  server_os    = "debian"
  server_image = "debian-10"
}

module "nginxletsencrypt-deploy" {
  source = "../../modules/nginx-letsencrypt"

  depends_on = [module.base]

  # Personal vault
  ssh_private_key = var.ssh_private_key
  server_domain = var.server_domain
  admin_email = var.admin_email

  # Change manually the following vars
  server_name  = "${module.base.server-name}"
  server_ip    = "${module.base.server-ip}"
}
