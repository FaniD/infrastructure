output "kubeone_api" {
  description = "kube-apiserver LB endpoint"

  value = module.k8s-cluster.kubeone_api
}

output "kubeone_hosts" {
  description = "Control plane endpoints to SSH to"

  value = module.k8s-cluster.kubeone_hosts
}

output "kubeone_workers" {
  description = "Workers definitions, that will be transformed into MachineDeployment object"

  value = module.k8s-cluster.kubeone_workers
}

