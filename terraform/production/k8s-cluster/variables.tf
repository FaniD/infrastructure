variable "hcloud_token" {
  description = "Hetzner API Token for Suite V1 project."
}

variable "ssh_private_key" {
  description = "Private key used by one's ssh agent"
}

variable "ssh_public_key_file" {
  description = "SSH public key file"
}

variable "ip_range" {
  description = "ip range to use for private network"
}

