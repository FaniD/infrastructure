terraform {
  backend "local" {
    path = "../../../local-vault/k8s-cluster/terraform.tfstate"
  }
}

module "k8s-cluster" {
  source = "../../modules/k8s-cluster"

  # Personal vault
  ssh_private_key = var.ssh_private_key
  ssh_public_key_file = var.ssh_public_key_file
  hcloud_token = var.hcloud_token

  # Shared vault
  ip_range = var.ip_range
}
