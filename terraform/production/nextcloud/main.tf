terraform {
  backend "local" {
    path = "../../../local-vault/nextcloud/terraform.tfstate"
  }
}

terraform {
  required_providers {
    hcloud = {
      source  = "hetznercloud/hcloud"
      version = ">= 1.20.1"
    }
  }
}

provider "hcloud" {
  token = var.hcloud_token
}

module "base" {
  source = "../../modules/server"

  providers = {
    hcloud = hcloud
  }

  # Shared vault
  net-id = var.net-id

  # Personal vault
  ssh_private_key = var.ssh_private_key
  hcloud_token    = var.hcloud_token

  # Change manually the following vars
  server_name  = "nextcloud"
  server_os    = "debian"
  server_image = "debian-11"
}

module "storage" {
  source = "../../modules/volume"

  depends_on = [module.base]

  providers = {
    hcloud = hcloud
  }

  # Shared vault
  luks_password = var.luks_password_storage

  # Personal vault
  ssh_private_key = var.ssh_private_key
  hcloud_token    = var.hcloud_token

  # Change manually the following vars
  volume_name = "nc-storage"
  volume_size = "10"
  mountpoint  = "/media/storage"

  # Server module outputs
  server_name     = "${module.base.server-name}"
  server-location = "${module.base.server-location}"
  server-id       = "${module.base.server-id}"
  server-ip       = "${module.base.server-ip}"
}

module "nextcloud" {
  source = "../../modules/nextcloud"

  depends_on = [module.storage]

  # Personal vault
  ssh_private_key = var.ssh_private_key
  server_domain = var.server_domain
  admin_email = var.admin_email
  full_url = var.full_url

  # Vault
  nextcloud_admin_password = var.nextcloud_admin_password
  postgresql_nextcloud_user_password = var.postgresql_nextcloud_user_password
  postgresql_postgres_password = var.postgresql_postgres_password
  redis_password = var.redis_password
  onlyoffice_secret = var.onlyoffice_secret
  coturn_static_auth_secret = var.coturn_static_auth_secret
  documentserver_db_user_password = var.documentserver_db_user_password
  nextcloud_users = var.nextcloud_users

  # Change manually the following vars
  server_name  = "${module.base.server-name}"
  server_ip    = "${module.base.server-ip}"
}

