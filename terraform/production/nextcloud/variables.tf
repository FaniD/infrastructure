# ==========================
# Defined in personal vault
# ==========================

# API token and ssh key per project and per user
variable "hcloud_token" {
  description = "Hetzner API Token for a project."
}

variable "ssh_private_key" {
  description = "Private key used by one's ssh agent."
}

# ========================
# Defined in shared vault
# ========================

# This value needs to change according the network id used in the project
# If network was created with global-resources go to local-vault/global-resources
# and run "teraform output"
# Otherwise find the net id through Hetzner / use hcloud cli -> hcloud network list
variable "net-id" {
  description = "Private network id to attach the server to"
}

variable "luks_password_storage" {
  description = "Luks password for volume storage"
}

variable "server_domain" {
  description = "Domain that will be assigned to the server"
}

variable "full_url" {
  description = "Domain's full url"
}

variable "admin_email" {
  description = "Admin email that the let's encrypt role will send notifications"
}

variable "nextcloud_admin_password" {
  description = "Password for the Nextcloud admin user"
}

variable "postgresql_nextcloud_user_password" {
  description = "Password for the PostgreSQL nextcloud user"
}

variable "postgresql_postgres_password" {
  description = "Password for the PostgreSQL postgres user"
}

variable "redis_password" {
  description = "Redis password"
}

variable "onlyoffice_secret" {
  description = "OnlyOffice secret"
}

variable "documentserver_db_user_password" {
  description = "Documentserver (onlyoffice) database user password"
}

variable "coturn_static_auth_secret" {
  description = "Auth for coturn"
}

variable "nextcloud_users" {
  type = map
  description = "All Nextcloud users for initial setup"
}

