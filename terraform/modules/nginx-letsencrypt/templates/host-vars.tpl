---

main_domain: '{{ vault_server_domain }}'
mail_domain: '{{ main_domain }}'

virtual_domains: '{{ ( [ main_domain, mail_domain | default(main_domain) ] + additional_virtual_domains | default([]) + additional_mail_domains | default([]) ) | unique }}'

letsencrypt_domains: '{{ vault_letsencrypt_domains }}'

letsencrypt_email: '{{ vault_admin_email }}'
