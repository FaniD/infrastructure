vault_server_domain: ${server_domain}
vault_admin_email: ${admin_email}

vault_letsencrypt_domains:
  ${server_domain}: [ ${server_domain} ]
