# Run let's encrypt and nginx ansible role on existing server

resource "null_resource" "fix-dns" {
  provisioner "local-exec" {
    command = "echo -e '\n\n-============== CREATE DNS A RECORD FOR DOMAIN ${var.server_domain} WITH IP: ${var.server_ip}. THEN RUN LOCALLY `touch /tmp/dnsdone`; I WILL WAIT HERE ==============-\n\n'; while ! test -f /tmp/dnsdone; do sleep 1; done"
  }

  provisioner "local-exec" {
    command = "rm /tmp/dnsdone"
  }
}

locals {
  rendered_ansible_playbook = templatefile("${path.module}/templates/ansible-playbook.tpl", {
    server_name = var.server_name
  })
  rendered_vault = templatefile("${path.module}/templates/vault.tpl", {
    server_domain = var.server_domain,
    admin_email   = var.admin_email
  })
}

resource "local_file" "host-vars" {
  content = file("${path.module}/templates/host-vars.tpl")
  filename = "../../../ansible/host_vars/${var.server_name}/nginx-letsencrypt"
}

resource "local_file" "ansible-playbook" {
  content = local.rendered_ansible_playbook
  filename = "../../../ansible/${var.server_name}-nginx-letsencrypt.yml"
}

resource "local_file" "vault" {
  content = local.rendered_vault
  filename = "../../../ansible/host_vars/${var.server_name}/nginx-letsencrypt-vault"
}

resource "null_resource" "update-playbook" {
  provisioner "local-exec" {
    command = "echo '    - nginx' >> ../../../ansible/${var.server_name}.yml"
  }
}

resource "null_resource" "encrypt-vault" {
  provisioner "local-exec" {
    command = "ansible-vault encrypt --vault-password-file=../../../local-vault/ansible_vault_password ${local_file.vault.filename}"
  }
}

resource "null_resource" "deploy_nginx_letsencrypt" {
  depends_on = [null_resource.update-playbook,local_file.ansible-playbook,local_file.host-vars,local_file.vault,null_resource.encrypt-vault]

  triggers = {
    host_vars        = local_file.host-vars.content,
    ansible_playbook = local_file.ansible-playbook.content,
    vault            = local_file.vault.content
  }

  provisioner "local-exec" {
    command = "ANSIBLE_CONFIG=../../../ansible/ansible.cfg ansible-playbook --private-key var.ssh_private_key --vault-password-file=../../../local-vault/ansible_vault_password ${local_file.ansible-playbook.filename} --inventory=../../../ansible/hosts --diff"
  }
}

resource "null_resource" "delete-playbook" {
  depends_on = [null_resource.deploy_nginx_letsencrypt]

  provisioner "local-exec" {
    command = "rm ../../../ansible/${var.server_name}-nginx-letsencrypt.yml"
  }
}
