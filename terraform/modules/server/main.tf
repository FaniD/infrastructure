# This module requires the provider resource to be defined in the calling module.
# This allows us to use dependencies among modules on a more complex module composition e.g. kubernetes cluster

# Create server resources and execute ansible playbook

resource "hcloud_server" "base" {
  name        = var.server_name
  server_type = var.server_machine_type
  image       = var.server_image
  location    = var.server_datacenter

  ssh_keys = ["fani"]

  connection {
    host        = self.ipv4_address
    type        = "ssh"
    user        = "root"
    private_key = file(var.ssh_private_key)
  }

  
  # Configuration, templating and ansible execution

  provisioner "remote-exec" {
    script = "${path.module}/scripts/update_and_create_deploy_user.sh"
  }
}

# Create ansible playbook and vars

locals {
  rendered_ansible_playbook = templatefile("${path.module}/templates/ansible-playbook.tpl", {
    server_name = var.server_name
  })
  rendered_vault = templatefile("${path.module}/templates/vault.tpl", {
    ansible_host = hcloud_server.base.ipv4_address
  })
}

resource "local_file" "ansible-playbook" {
  content = local.rendered_ansible_playbook
  filename = "../../../ansible/${var.server_name}.yml"
}

resource "local_file" "host-vars" {
  content = file("${path.module}/templates/host-vars.tpl")
  filename = "../../../ansible/host_vars/${var.server_name}/all"
}

resource "local_file" "vault" {
  content = local.rendered_vault
  filename = "../../../ansible/host_vars/${var.server_name}/base-vault"
}

resource "null_resource" "encrypt-vault" {
  provisioner "local-exec" {
    command = "ansible-vault encrypt --vault-password-file=../../../local-vault/ansible_vault_password ${local_file.vault.filename}"
  }
}

# Create private network interface on server
resource "hcloud_server_network" "priv_net_interface" {
  depends_on = [hcloud_server.base]

  server_id  = hcloud_server.base.id
  network_id = var.net-id
}

resource "null_resource" "update-ansible-hosts" {
  depends_on = [hcloud_server.base]

  provisioner "local-exec" {
    command = "sed --in-place '/\\[${var.server_os}]/a ${var.server_name}' ../../../ansible/hosts"
  }
}

resource "null_resource" "base_server_setup" {
  depends_on = [null_resource.update-ansible-hosts,local_file.ansible-playbook,local_file.host-vars,local_file.vault,null_resource.encrypt-vault]

  triggers = {
    ansible_vault    = local_file.vault.content,
    host_vars        = local_file.host-vars.content,
    ansible_playbook = local_file.ansible-playbook.content
  }

  provisioner "local-exec" {
    command = "ANSIBLE_CONFIG=../../../ansible/ansible.cfg ansible-playbook --private-key var.ssh_private_key --vault-password-file=../../../local-vault/ansible_vault_password ${local_file.ansible-playbook.filename} --inventory=../../../ansible/hosts --diff"
  }
}

