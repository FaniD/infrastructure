---

ansible_host: '{{ vault_ansible_host }}'

boilerplate_packages:
  - aptitude
  - bind9-host
  - bash-completion
  - curl
  - deborphan
  - fd-find
  - git
  - htop
  - lsof
  - ncdu
  - nethogs
  - neovim
  - nmap
  - openssh-server
  - python3-psycopg2
  - ripgrep
  - rsync
  - sudo
  - tmux
  - tor
  - whois
