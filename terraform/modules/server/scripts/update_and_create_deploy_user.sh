#!/bin/sh

apt update
apt full-upgrade -y
apt install -y tmux neovim aptitude python3

adduser --disabled-password --gecos '' deploy
adduser deploy sudo

cp -a /root/.ssh/ /home/deploy
chown -R deploy.deploy /home/deploy/

update-alternatives --set editor /usr/bin/nvim

sed -i 's/%sudo\tALL=(ALL:ALL) ALL/%sudo\tALL=(ALL:ALL) NOPASSWD : ALL/' /etc/sudoers
