output "server-id" {
  value       = hcloud_server.base.id
  description = "Server id on hetzner"
}

output "server-name" {
  value = var.server_name
  description = "Server name. This output is usually used on production to force dependencies between modules"
}

output "server-ip" {
  value       = hcloud_server.base.ipv4_address
  description = "Server public ip address"
}

output "server-priv-ip" {
  value = hcloud_server_network.priv_net_interface.ip
  description = "Server private ip address"
}

output "server-location" {
  value = hcloud_server.base.location
  description = "Server location on Hetzner - need it for multiple volumes to be attached"
}

