variable "project_name" {
  description = "prefix for cloud resources"
  default     = "static-infra"
}

variable "server_name" {
  description = "Server's name"
  default     = "random-server"
}

variable "hcloud_token" {
  description = "Hetzner API Token"
}

variable "server_machine_type" {
  default     = "cpx11"
  description = "Hetzner machine type used for this server"
}

variable "server_datacenter" {
  default = "fsn1"
}

variable "server_os" {
  default = "debian"
  description = "Choose between debian or ubuntu. If you need a control plane node for K8s, go for ubuntu"
}

variable "server_image" {
  default     = "debian-10"
  description = "Server's OS image"
}

variable "net-id" {
  description = "Network id of the Hetzner Private Network created by global_resources environment"
}

variable "ssh_private_key" {
  description = "Private key used by one's ssh agent"
  default     = "~/.ssh/id_rsa"
}
