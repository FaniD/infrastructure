/*
Copyright 2019 The KubeOne Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

# Made some changes on the origin code of KubeOne to fit
# my k8s resources as defined on my module

output "kubeone_api" {
  description = "kube-apiserver LB endpoint"

  value = {
    endpoint = module.load-balancer.server-ip
  }
}

output "kubeone_hosts" {
  description = "Control plane endpoints to SSH to"

  value = {
    control_plane = {
      cluster_name         = var.cluster_name
      cloud_provider       = "hetzner"
      private_address      = [module.control-plane-1.server-priv-ip, module.control-plane-2.server-priv-ip, module.control-plane-3.server-priv-ip]
      public_address       =  [module.control-plane-1.server-ip, module.control-plane-2.server-ip, module.control-plane-3.server-ip]
      network_id           = module.network.net-id
      ssh_agent_socket     = var.ssh_agent_running ? var.ssh_agent_socket : ""
      ssh_port             = var.ssh_port
      ssh_private_key_file = var.ssh_agent_running ? "" : var.ssh_private_key
      ssh_user             = var.ssh_username
    }
  }
}

output "kubeone_workers" {
  description = "Workers definitions, that will be transformed into MachineDeployment object"

  value = {
    # following outputs will be parsed by kubeone and automatically merged into
    # corresponding (by name) worker definition
    "${var.cluster_name}-pool1" = {
      replicas = var.workers_replicas
      providerSpec = {
        sshPublicKeys   = [file(var.ssh_public_key_file)]
        operatingSystem = var.worker_os
        operatingSystemSpec = {
          distUpgradeOnBoot = false
        }
        cloudProviderSpec = {
          # provider specific fields:
          # see example under `cloudProviderSpec` section at:
          # https://github.com/kubermatic/machine-controller/blob/master/examples/hetzner-machinedeployment.yaml
          serverType = var.worker_type
          location   = var.worker_datacenter
          image      = var.worker_image
          networks = [
            module.network.net-id
          ]
          # Datacenter (optional)
          # datacenter = ""
          labels = {
            "${var.cluster_name}-workers" = "pool1"
          }
        }
      }
    }
  }
}

