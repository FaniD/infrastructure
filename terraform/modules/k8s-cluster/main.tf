terraform {
  required_providers {
    hcloud = {
      source  = "hetznercloud/hcloud"
      version = ">= 1.20.1"
    }
    null = {
      source = "hashicorp/null"
    }
  }
}

provider "hcloud" {
  token = var.hcloud_token
}

# Create network

module "network" {
  source = "../network"

  # Personal vault
  hcloud_token    = var.hcloud_token
  ssh_private_key = var.ssh_private_key

  # Shared vault
  project_name = "kubernetes"
  ip_range     = var.ip_range
}


# Create control plane nodes

module "control-plane-1" {
  providers = {
    hcloud = hcloud
  }

  depends_on = [module.network]
  source     = "../server"

  net-id              = module.network.net-id
  server_name         = "k8s-control-plane-1"
  server_os           = "ubuntu"
  server_image        = "ubuntu-20.04"
  server_machine_type = "cx21"

  # Personal vault
  hcloud_token    = var.hcloud_token
  ssh_private_key = var.ssh_private_key
}

module "control-plane-2" {
  providers = {
    hcloud = hcloud
  }

  depends_on = [module.control-plane-1]
  source     = "../server"

  net-id              = module.network.net-id
  server_name         = "k8s-control-plane-2"
  server_os           = "ubuntu"
  server_image        = "ubuntu-20.04"
  server_machine_type = "cx21"

  # Personal vault
  hcloud_token    = var.hcloud_token
  ssh_private_key = var.ssh_private_key
}

module "control-plane-3" {
  providers = {
    hcloud = hcloud
  }

  depends_on = [module.control-plane-2]
  source     = "../server"

  net-id              = module.network.net-id
  server_name         = "k8s-control-plane-3"
  server_os           = "ubuntu"
  server_image        = "ubuntu-20.04"
  server_machine_type = "cx21"

  # Personal vault
  hcloud_token    = var.hcloud_token
  ssh_private_key = var.ssh_private_key
}


# Create load balancer

module "load-balancer" {
  providers = {
    hcloud = hcloud
  }

  depends_on = [module.control-plane-3]
  source     = "../server"

  net-id              = module.network.net-id
  server_name         = "k8s-load-balancer"
  server_os           = "ubuntu"
  server_image        = "ubuntu-20.04"
  server_machine_type = "cx11"

  # Personal vault
  hcloud_token    = var.hcloud_token
  ssh_private_key = var.ssh_private_key
}

locals {
  rendered_lb_config = templatefile("${path.module}/templates/etc_gobetween.tpl", {
    lb_targets = [module.control-plane-1.server-ip, module.control-plane-2.server-ip, module.control-plane-3.server-ip]
    lb_address = module.load-balancer.server-ip
  })
}

resource "null_resource" "load_balancer_configuration" {
  depends_on = [module.load-balancer]

  triggers = {
    config = local.rendered_lb_config
  }

  connection {
    type        = "ssh"
    host        = module.load-balancer.server-ip
    user        = "root"
    private_key = file(var.ssh_private_key)
  }

  provisioner "remote-exec" {
    script = "${path.module}/scripts/gobetween.sh"
  }

  provisioner "file" {
    content     = local.rendered_lb_config
    destination = "/tmp/gobetween.toml"
  }

  provisioner "remote-exec" {
    inline = [
      "sudo cp /tmp/gobetween.toml /etc/gobetween.toml",
      "sudo systemctl restart gobetween",
    ]
  }
}
