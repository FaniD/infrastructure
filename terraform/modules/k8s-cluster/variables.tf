variable "hcloud_token" {
  description = "Hetzner API Token"
}

variable "ip_range" {
  description = "ip range to use for private network"
}

variable "ssh_private_key" {
  description = "Private key used by one's ssh agent"
  default     = "~/.ssh/id_rsa"
}

variable "cluster_name" {
  description = "prefix for cloud resources"
  default = "kubernetes"
}

variable "worker_os" {
  description = "OS to run on worker machines"

  # valid choices are:
  # * ubuntu
  # * centos
  default = "ubuntu"
}

variable "ssh_public_key_file" {
  description = "SSH public key file"
  default     = "~/.ssh/id_rsa.pub"
}

variable "ssh_port" {
  description = "SSH port to be used to provision instances"
  default     = 22
}

variable "ssh_username" {
  description = "SSH user, used only in output"
  default     = "deploy"
}

variable "ssh_agent_running" {
  description = "SSH Agent is running"
  default     = false
}

variable "ssh_agent_socket" {
  description = "SSH Agent socket, default to grab from $SSH_AUTH_SOCK"
  default     = "env:SSH_AUTH_SOCK"
}

variable "worker_type" {
  default = "cx21"
}

variable "worker_datacenter" {
  default = "fsn1"
}

variable "worker_image" {
  default     = "ubuntu-20.04"
  description = "Server's OS image"
}

variable "workers_replicas" {
  default = 1
}
