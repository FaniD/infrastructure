terraform {
  required_providers {
    hcloud = {
      source  = "hetznercloud/hcloud"
      version = ">= 1.20.1"
    }
  }
}

provider "hcloud" {
  token = var.hcloud_token
}

resource "hcloud_network" "priv-net" {
  name     = var.project_name
  ip_range = var.ip_range
}

resource "hcloud_network_subnet" "priv-subnet" {
  network_id   = hcloud_network.priv-net.id
  type         = "server"
  network_zone = var.network_zone
  ip_range     = var.ip_range
}

resource "hcloud_ssh_key" "ssh_keys" {
  for_each   = var.ssh_public_keys
  name       = each.key
  public_key = each.value
}
