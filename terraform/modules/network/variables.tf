variable "project_name" {
  description = "prefix for cloud resources"
  default     = "static-infra"
}

variable "hcloud_token" {
  description = "Hetzner API Token"
}

variable "ip_range" {
  description = "ip range to use for private network"
}

variable "network_zone" {
  default     = "eu-central"
  description = "network zone to use for private network"
}

variable "ssh_private_key" {
  description = "Private key used by one's ssh agent"
  default     = "~/.ssh/id_rsa"
}

variable "ssh_public_keys" {
  type = map

  default = {
    fani = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAINFsXvrdKp8VnW2pujFAwj/GzL9gJ70qBLNLletlDC4F me@fanid.gr"
  }
}
