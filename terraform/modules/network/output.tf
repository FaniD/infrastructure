output "net-id" {
  value       = hcloud_network.priv-net.id
  description = "Hetzner Private Network used on a project"
}

