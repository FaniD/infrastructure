# Run hashicorp vault ansible role on existing server
# Step 1: Installation
# Step 2: Creates backend specified on host-vars

locals {
  rendered_ansible_playbook = templatefile("${path.module}/templates/ansible-playbook.tpl", {
    server_name = var.server_name
  })
  rendered_vault = templatefile("${path.module}/templates/vault.tpl", {
    tls_src_files = var.tls_src_files,
    server_domain = var.server_domain,
    admin_email   = var.admin_email
  })
}

resource "local_file" "host-vars" {
  content = file("${path.module}/templates/host-vars-file-backend.tpl")
  filename = "../../../ansible/host_vars/${var.server_name}/hashicorp-vault"
}

resource "local_file" "ansible-playbook" {
  content = local.rendered_ansible_playbook
  filename = "../../../ansible/${var.server_name}-hashicorp-vault.yml"
}

resource "local_file" "vault" {
  content = local.rendered_vault
  filename = "../../../ansible/host_vars/${var.server_name}/hashicorp-vault-secret-vars"
}

resource "null_resource" "update-playbook" {
  provisioner "local-exec" {
    command = "echo '    - hashicorp-vault' >> ../../../ansible/${var.server_name}.yml"
  }
}

resource "null_resource" "encrypt-vault" {
  provisioner "local-exec" {
    command = "ansible-vault encrypt --vault-password-file=../../../local-vault/ansible_vault_password ${local_file.vault.filename}"
  }
}

resource "null_resource" "hashicorp_vault_setup" {
  depends_on = [null_resource.update-playbook,local_file.ansible-playbook,local_file.host-vars,local_file.vault,null_resource.encrypt-vault]

  triggers = {
    host_vars        = local_file.host-vars.content,
    ansible_playbook = local_file.ansible-playbook.content,
    vault            = local_file.vault.content
  }

  provisioner "local-exec" {
    command = "ANSIBLE_CONFIG=../../../ansible/ansible.cfg ansible-playbook --private-key var.ssh_private_key --vault-password-file=../../../local-vault/ansible_vault_password ${local_file.ansible-playbook.filename} --inventory=../../../ansible/hosts --diff"
  }
}

resource "null_resource" "delete-playbook" {
  depends_on = [null_resource.hashicorp_vault_setup]

  provisioner "local-exec" {
    command = "rm ../../../ansible/${var.server_name}-hashicorp-vault.yml"
  }
}
