variable "server_name" {
  description = "Server's name"
  default     = "random-server"
}

variable "ssh_private_key" {
  description = "Private key used by one's ssh agent"
  default     = "~/.ssh/id_rsa"
}

variable "server_domain" {
  description = "Domain that will be assigned to the server"
}

variable "tls_src_files" {
  description = "Path to tls source files"
}

variable "admin_email" {
  description = "Admin email that the let's encrypt role will send notifications"
}

