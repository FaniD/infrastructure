---

- name: basic configuration for ALL the hosts
  hosts: ${server_name}
  remote_user: deploy
  become: yes

  roles:
    - hashicorp-vault
