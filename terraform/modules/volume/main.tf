# This module requires the provider resource to be defined in the calling module.
# This allows us to use dependencies among modules on a more complex module composition e.g. kubernetes cluster

resource "hcloud_volume" "volume" {
  name = var.volume_name
  size = var.volume_size
  location = var.server-location
}

resource "hcloud_volume_attachment" "volume-attach" {
  volume_id = hcloud_volume.volume.id
  server_id = var.server-id
  automount = false
}

locals {
  rendered_vault = templatefile("${path.module}/templates/vault.tpl", {
    luks_password = var.luks_password,
    volume_name = var.volume_name
  })
}

resource "null_resource" "append_host_vars" {
  provisioner "local-exec" {
    command = "echo 'luks_password_${var.volume_name}: \"{{ vault_luks_password_${var.volume_name} }}\"' >> ../../../ansible/host_vars/${var.server_name}/all"
  }
}

resource "local_file" "vault" {
  content = local.rendered_vault
  filename = "../../../ansible/host_vars/${var.server_name}/${var.volume_name}-vault"
}

resource "null_resource" "encrypt-vault" {
  provisioner "local-exec" {
    command = "ansible-vault encrypt --vault-password-file=../../../local-vault/ansible_vault_password ${local_file.vault.filename}"
  }
}

resource "null_resource" "luks_encryption" {
  depends_on = [hcloud_volume_attachment.volume-attach,local_file.vault,null_resource.encrypt-vault,null_resource.append_host_vars]

  connection {
    type = "ssh"
    host = var.server-ip
    user = "root"
    private_key = file(var.ssh_private_key)
  }

  triggers = {
    ansible_vault = local_file.vault.content
  }

  provisioner "file" {
    source = "${path.module}/scripts/setup-cryptsetup.sh"
    destination = "/tmp/setup-cryptsetup.sh"
  }

  provisioner "remote-exec" {
    inline = [
      "chmod +x /tmp/setup-cryptsetup.sh",
      "/tmp/setup-cryptsetup.sh ${var.luks_password} ${hcloud_volume.volume.linux_device} ${var.mountpoint} ${var.volume_name}"
    ]
  }
}

