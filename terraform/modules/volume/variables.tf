variable "project_name" {
  description = "prefix for cloud resources"
  default     = "static-infra"
}

variable "volume_name" {
  description = "Server's volume name"
  default     = "storage"
}

variable "server-id" {
  description = "Server id on Hetzner"
}

variable "server-location" {
  description = "Server location on Hetzner - needed for multiple volume attachments"
}

variable "server-ip" {
  description = "Server's private ip address"
}

variable "volume_size" {
  description = "Size of the volume"
  default = 10
}

variable "hcloud_token" {
  description = "Hetzner API Token"
}

variable "ssh_private_key" {
  description = "Private key used by one's ssh agent"
  default     = "~/.ssh/id_rsa"
}

variable "luks_password" {
  description = "Luks password for volume encryption"
}

variable "mountpoint" {
  description = "Mountpoint where the block device will be mounted"
}

variable "server_name" {
  description = "Server name to which the volume will be attached"
}
