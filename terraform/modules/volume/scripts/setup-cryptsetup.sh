#!/bin/bash

# Usage: ./setup-cryptsetup.sh arg1 arg2 arg3
# arg1: luks password - determined by user
# arg2: block device id - output of terraform module volume
# arg3: path to mountpoint - determined by you. It must be an absolute path!
# arg4: volume name

# Block device
BLOCK_DEVICE=$(readlink -f ${2})
MOUNTPOINT=${3}
VOL_NAME=${4}

# postgresql is installed via boilerplate (base server), so it can be disabled later on. if this step would be forgotten in a later installation, it would be bad.
apt install -y cryptsetup postgresql

# Give luks password
echo -n "$1" | cryptsetup luksFormat "$BLOCK_DEVICE"

# Detect block device by-uuid by serial searching (O(n))
for d in /dev/disk/by-uuid/*; do
    if [ `readlink -f "$d"` = "$BLOCK_DEVICE" ]; then
        BLOCK_DEVICE="$d"
    fi
done

# Create mountpoint
mkdir -p ${MOUNTPOINT}

# Create crypt device and format it
echo -n "$1" | cryptsetup luksOpen "$BLOCK_DEVICE" crypt-${VOL_NAME}
mkfs.ext4 /dev/mapper/crypt-${VOL_NAME}
mount /dev/mapper/crypt-${VOL_NAME} ${MOUNTPOINT}

systemctl stop postgresql.service
systemctl disable postgresql.service

mkdir -p /var/lib/postgresql
mkdir -p ${MOUNTPOINT}/postgresql

mv /var/lib/postgresql/* ${MOUNTPOINT}/postgresql/
mount --bind ${MOUNTPOINT}/postgresql/ /var/lib/postgresql/
systemctl start postgresql.service

cat > /home/deploy/mount-hetzner-crypt.sh <<EOF
#! /bin/sh

sudo mkdir -p ${MOUNTPOINT}

echo "opening and mounting device:"
sudo cryptsetup luksOpen "$BLOCK_DEVICE" crypt-${VOL_NAME}
sudo mount /dev/mapper/crypt-${VOL_NAME} ${MOUNTPOINT}
sudo mount --bind ${MOUNTPOINT}/postgresql /var/lib/postgresql

# this may fail, so they come last to not affect all the other steps
sudo systemctl start postgresql.service
EOF

chown -R deploy.deploy /home/deploy/
chmod 750 /home/deploy/mount-hetzner-crypt.sh

