vault_server_domain: ${server_domain}
vault_admin_email: ${admin_email}

vault_letsencrypt_domains:
  ${server_domain}: [ ${server_domain} ]

vault_full_url: ${full_url}
vault_nextcloud_admin_password: ${nextcloud_admin_password}
vault_postgresql_nextcloud_user_password: ${postgresql_nextcloud_user_password}
vault_postgresql_postgres_password: ${postgresql_postgres_password}
vault_redis_password: ${redis_password}
vault_onlyoffice_secret: ${onlyoffice_secret}
vault_documentserver_db_user_password: ${documentserver_db_user_password}
vault_coturn_static_auth_secret: ${coturn_static_auth_secret}
vault_nextcloud_users:
%{ for username, user_info in nextcloud_users ~}
  ${username}: { ${user_info}, password: '{{ lookup("password", "/dev/null length=16") }}' }
%{ endfor ~}
