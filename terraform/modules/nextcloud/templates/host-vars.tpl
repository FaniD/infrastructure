---

main_domain: '{{ vault_server_domain }}'
mail_domain: '{{ vault_server_domain }}'

virtual_domains: '{{ vault_server_domain }}'
virtual_domain_name: '{{ vault_server_domain }}'
#virtual_domains: '{{ ( [ main_domain, mail_domain | default(main_domain), postfix_domain|default(main_domain), dovecot_domain|default(main_domain) ] + additional_virtual_domains | default([]) + additional_mail_domains | default([]) + nextcloud_domains | default([]) + djapp_domains | default([]) ) | unique }}'

#additional_virtual_domains: []

### letsencrypt variables

letsencrypt_domains: '{{ vault_letsencrypt_domains }}'
letsencrypt_email: '{{ vault_admin_email }}'

nextcloud_domains: [ '{{ vault_server_domain}}' ]

nextcloud_users: '{{ vault_nextcloud_users }}'

nextcloud_admin_password: '{{ vault_nextcloud_admin_password }}'
postgresql_nextcloud_user_password: '{{ vault_postgresql_nextcloud_user_password }}'
postgresql_postgres_password: '{{ vault_postgresql_postgres_password }}'
redis_password: '{{ vault_redis_password }}'
coturn_static_auth_secret: '{{ vault_coturn_static_auth_secret }}'

onlyoffice_secret: '{{ vault_onlyoffice_secret }}'

documentserver_db_user_password: '{{ vault_documentserver_db_user_password }}'

additional_nextcloud_apps:
  - onlyoffice
  - spreed

auto_backups:
  - disable

nextcloud_theming_color: '#9a58cc'
nextcloud_theming_name: 'Nextcloud provided by FaniD'
nextcloud_theming_slogan: '{{ vault_full_url }}'

nextcloud_apps:
  - apporder
  - contacts
  - calendar
  - tasks
  - notes
  - deck
  - integration_whiteboard
  - polls
  - music
  - twofactor_backupcodes
  - twofactor_totp
  - twofactor_u2f
