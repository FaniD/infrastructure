variable "server_name" {
  description = "Server's name"
  default     = "random-server"
}

variable "ssh_private_key" {
  description = "Private key used by one's ssh agent"
  default     = "~/.ssh/id_rsa"
}

variable "server_domain" {
  description = "Domain that will be assigned to the server"
}

variable "server_ip" {
  description = "Public IP of server"
}

variable "admin_email" {
  description = "Admin email that the let's encrypt role will send notifications"
}

variable "full_url" {
  description = "Domain's full url"
}

variable "nextcloud_admin_password" {
  description = "Password for the Nextcloud admin user"
}

variable "postgresql_nextcloud_user_password" {
  description = "Password for the PostgreSQL nextcloud user"
}

variable "postgresql_postgres_password" {
  description = "Password for the PostgreSQL postgres user"
}

variable "redis_password" {
  description = "Redis password"
}

variable "coturn_static_auth_secret" {
  description = "Auth for coturn"
}

variable "onlyoffice_secret" {
  description = "OnlyOffice secret"
}

variable "documentserver_db_user_password" {
  description = "Documentserver (onlyoffice) database user password"
}

variable "nextcloud_users" {
  type = map
  description = "All Nextcloud users for initial setup"
}

