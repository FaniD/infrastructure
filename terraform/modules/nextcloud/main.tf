resource "null_resource" "fix-dns" {
  provisioner "local-exec" {
    command = "echo -e '\n\n-============== CREATE DNS A RECORD FOR DOMAIN ${var.server_domain} WITH IP: ${var.server_ip}. THEN RUN LOCALLY `touch /tmp/dnsdone`; I WILL WAIT HERE ==============-\n\n'; while ! test -f /tmp/dnsdone; do sleep 1; done"
  }

  provisioner "local-exec" {
    command = "rm /tmp/dnsdone"
  }
}

locals {
  rendered_ansible_playbook = templatefile("${path.module}/templates/ansible-playbook.tpl", {
    server_name = var.server_name
  })
  rendered_vault = templatefile("${path.module}/templates/vault.tpl", {
    server_domain = var.server_domain,
    full_url = var.full_url,
    admin_email   = var.admin_email,
    nextcloud_admin_password = var.nextcloud_admin_password,
    postgresql_nextcloud_user_password = var.postgresql_nextcloud_user_password,
    postgresql_postgres_password = var.postgresql_postgres_password,
    redis_password = var.redis_password,
    onlyoffice_secret = var.onlyoffice_secret,
    documentserver_db_user_password = var.documentserver_db_user_password,
    coturn_static_auth_secret = var.coturn_static_auth_secret,
    nextcloud_users = var.nextcloud_users
  })
}

resource "local_file" "host-vars" {
  content = file("${path.module}/templates/host-vars.tpl")
  filename = "../../../ansible/host_vars/${var.server_name}/nextcloud"
}

resource "local_file" "ansible-playbook" {
  content = local.rendered_ansible_playbook
  filename = "../../../ansible/${var.server_name}-nextcloud.yml"
}

resource "local_file" "vault" {
  content = local.rendered_vault
  filename = "../../../ansible/host_vars/${var.server_name}/nextcloud-vault"
}

resource "null_resource" "update-playbook" {
  provisioner "local-exec" {
    command = "echo -n '    - nextcloud\n    - coturn' >> ../../../ansible/${var.server_name}.yml"
  }
}

resource "null_resource" "encrypt-vault" {
  provisioner "local-exec" {
    command = "ansible-vault encrypt --vault-password-file=../../../local-vault/ansible_vault_password ${local_file.vault.filename}"
  }
}

resource "null_resource" "nextcloud_setup" {
  depends_on = [null_resource.update-playbook,local_file.ansible-playbook,local_file.host-vars,local_file.vault,null_resource.encrypt-vault]

  triggers = {
    host_vars        = local_file.host-vars.content,
    ansible_playbook = local_file.ansible-playbook.content,
    vault            = local_file.vault.content
  }

  provisioner "local-exec" {
    command = "ANSIBLE_CONFIG=../../../ansible/ansible.cfg ansible-playbook --private-key var.ssh_private_key --vault-password-file=../../../local-vault/ansible_vault_password ${local_file.ansible-playbook.filename} --inventory=../../../ansible/hosts --diff"
  }
}

resource "null_resource" "delete-playbook" {
  depends_on = [null_resource.nextcloud_setup]

  provisioner "local-exec" {
    command = "rm ../../../ansible/${var.server_name}-nextcloud.yml"
  }
}
